Usage


Windows
C:/> manduka.bat <language> <file location>

Linux\ Mac
$ sh ./manduka.sh <language> <file location>

The XML reports can be found in the reports directory

Use the report-viewer.html to view XML reports in a more readable format